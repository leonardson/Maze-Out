
function startGame() {
    player = new component(35, 60, "img/player.png", 35, 40);
    obstacle = new component(60, 60, "img/brick.png", 85, 40);
    myGameArea.start();

    window.addEventListener('keydown', function(event) {
        if (event.key === "ArrowRight") {
            player.move('right')
        }
        else if (event.key === "ArrowLeft") {
            player.move('left')
        }
        else if (event.key === "ArrowUp") {
            player.move('up')
        }
        else if (event.key === "ArrowDown") {
            player.move('down')
        }
    })
}

var myGameArea = {
    canvas: document.createElement('canvas'),
    height: window.innerHeight,
    width: window.innerWidth,
    start: function () {
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.context = this.canvas.getContext('2d');
        this.canvas.style = "background-color: #CCC";
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);

        this.interval = setInterval(function () {
            myGameArea.clear();
            obstacle.update();
            if (obstacle.crashWith(player)) {
                myGameArea.stop();
            }
            player.update();
            player.move(player.actualMove)
        }, 20);
    },
    clear: function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    stop: function () {
        clearInterval(this.interval)
    }
}

function component(width, height, image, x, y) {
    // Params of block
    this.image = new Image();
    this.image.src = image;
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    this.update = function() {
        ctx = myGameArea.context;
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }

    // Movement
    this.speed = 20
    this.actualMove = ''
    this.move = function (direction) {
        if (direction == 'up') {
            this.y -= this.speed
        }
        else if (direction == 'down') {
            this.y += this.speed
        }
        else if (direction == 'right') {
            this.x += this.speed
        }
        else if (direction == 'left') {
            this.x -= this.speed
        }
        this.actualMove = direction
        this.checkPositionIsValid()
    }
}
component.prototype.checkPositionIsValid = function () {
    if (this.x <= 0) {
        this.x = 0;
    }
    else if (this.x >= (myGameArea.width - this.width)) {
        this.x = (myGameArea.width - this.width)
    }
    if (this.y <= 0) {
        this.y = 0;
    }
    else if (this.y >= (myGameArea.height - this.height)) {
        this.y = (myGameArea.height - this.height)
    }
}

component.prototype.crashWith = function(otherobj) {
    var myleft = this.x;
    var myright = this.x + (this.width);
    var mytop = this.y;
    var mybottom = this.y + (this.height);
    var otherleft = otherobj.x;
    var otherright = otherobj.x + (otherobj.width);
    var othertop = otherobj.y;
    var otherbottom = otherobj.y + (otherobj.height);
    var crash = true;
    if ((mybottom <= othertop) || (mytop > otherbottom) ||
        (myright < otherleft) || (myleft > otherright)) {
        crash = false;
    }
    return crash;
}